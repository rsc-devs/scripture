<?php

namespace Drupal\scripture;

/**
 * Book class.
 *
 * Represents a book loaded from the DB.
 *
 * @package Drupal\scripture
 */
class Book {

  /**
   * The ordinal that identifies this book in the translation.
   *
   * @var int
   */
  public $number;

  /**
   * The abbreviation of the translation to which this book belongs.
   *
   * This is a unique key, identifying the translation in the DB.
   *
   * @var string
   */
  public $translationAbbr;

  /**
   * The 'friendly' name of the book, used in the GUI.
   *
   * @var string
   */
  public $name;

  /**
   * Map DB column names to object properties.
   *
   * This magic function is called when the DatabaseHelper tries to set
   * inaccessible properties on a Translation object. That happens when the name
   * of the property in the DB (which generally follows snake_case_naming) is
   * not the same as the name of the property in this class (which should follow
   * camelCaseNaming).
   *
   * @param string $name
   *   The name of the property (column name) being set.
   * @param mixed $value
   *   The value of the property being set.
   */
  public function __set($name, $value) {
    switch ($name) {
      case 'booknum':
        $this->number = $value;
        break;

      case 'translation':
        $this->translationAbbr = $value;
        break;

      case 'bookname':
        $this->name = $value;
        break;
    }
  }

  /**
   * Get the translation of the current book.
   *
   * @return \Drupal\scripture\Translation
   *   The translation in which this book appears.
   */
  public function getTranslation() {
    return DatabaseHelper::getTranslation($this->translationAbbr);
  }

  public function getAbbreviations() {
    // todo
  }

  public function getChapters() {
    // todo?
  }

}
