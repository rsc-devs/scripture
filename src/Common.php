<?php

namespace Drupal\scripture;

use Drupal\Core\Url;

/**
 * Class Common.
 *
 * @package Drupal\scripture
 */
class Common {

  const SHOW_TEXT_NO = 1;
  const SHOW_TEXT_TRUNC = 2;
  const SHOW_TEXT_ALL = 3;

  /**
   * Notify the user that the default translation is not specified.
   *
   * For best practices concerning links inside translated text, see
   * https://www.drupal.org/docs/7/api/localization-api/dynamic-or-static-links-and-html-in-translatable-strings .
   */
  public static function showTranslationError() {
    drupal_set_message(
      t(
        "The default translation is not specified.
Don't know which text to load from database.
Visit <a href='@href'>the settings page</a> to correct this.",
        [
          '@href' => Url::fromRoute('scripture.config.translations')->toString(),
        ]
      ),
      'error',
      FALSE
    );
  }

  /**
   * Decode a verse range represented as a string.
   *
   * @param string $str
   *   Numeric verse id range, like "123-205".
   *
   * @return array|null
   *   An array with keys from_vid and to_vid, or null on failure.
   */
  public static function decodeVerseRange($str) {

    $matches = array();
    if (!preg_match('/^(\d+)(-(\d+))?$/', $str, $matches)) {
      // Regular expression did not match.
      return NULL;
    }
    // Looks valid, i.e. either "1234" or "1234-5678".
    if (empty($matches[1])) {
      // Invalid starting verse.
      return NULL;
    }
    // Starting verse is specified.
    $from_vid = $matches[1];
    if (!empty($matches[3])) {
      // Ending verse is specified.
      $to_vid = $matches[3];
    }
    else {
      // Use the starting verse for the ending verse.
      $to_vid = $from_vid;
    }

    return array(
      'from_vid' => $from_vid,
      'to_vid'   => $to_vid,
    );

  }

}
