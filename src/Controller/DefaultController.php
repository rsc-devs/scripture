<?php

namespace Drupal\scripture\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\scripture\Common;
use Drupal\scripture\DatabaseHelper;

/**
 * Default controller for the scripture module.
 */
class DefaultController extends ControllerBase {

  /**
   * Callback for the status page.
   *
   * @return array
   *   Renderable status page.
   */
  public function status() {
    $content = [];

    $translations = DatabaseHelper::getTranslations();

    $translation_rows = [];
    foreach ($translations as $translation) {
      /* @var \Drupal\scripture\Translation $translation */
      $translation_rows[] = [
        $translation->name,
        count($translation->getBooks()),
      ];
    }

    $content['summary'] = [
      '#prefix' => '<h2>' . t('Summary') . '</h2>',
      '#theme' => 'table',
      '#rows' => [
        [
          t('Number of translations'),
          count($translations),
        ],
      ],
    ];

    if (count($translation_rows)) {
      $content['translations'] = [
        '#prefix' => '<h2>' . t('Translations') . '</h2>',
        '#theme' => 'table',
        '#header' => [
          t('Translation'),
          t('Number of books'),
        ],
        '#rows' => $translation_rows,
      ];
    }

    $content['TODO'] = [
      '#prefix' => '<h2>TODO</h2>',
      '#markup' => 'TODO: display any other required statistics here',
    ];

    return $content;
  }

  /**
   * Callback for the page that lists all content filtered by scripture.
   *
   * @param string $param
   *   Numeric verse id range, like "123-205".
   *
   * @return array
   *   A renderable page.
   */
  public function nodeListing($param = '') {
    $content = [];

    $content['verse_picker'] = \Drupal::formBuilder()->getForm('\Drupal\scripture\Form\VersePickerForm');
    $content['verse_picker']['submit']['#value'] = $this->t("Show nodes");
    // TODO: experiment with using another form inside a normal page callback.
    // TODO: somehow get the submitted values. Maybe override the submit handler
    // and use a function that stores the values in the $_SESSION . Then
    // customise this page based on those values.
    // TODO: The easier option will be to simply make another form, and display
    // the nodes at the bottom of the form after submission (always using
    // $form_state->setRebuild() in the submit handler).
    // TODO: OR override the submit handler and redirect to this page, but with
    // an argument, so we can show both the form and the nodes.
    if (empty($param)) {
      $content['summary'] = [
        '#type' => 'markup',
        '#markup' => 'TODO: show the number of nodes per book and/or translation.',
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
    }
    else {
      $range = Common::decodeVerseRange($param);
      $range['translation'] = \Drupal::config('scripture.settings')
        ->get('default_translation');
      // TODO: check if range is valid before calling preview.
      $content['scripture'] = scripture_preview($range);
    }

    return $content;
  }

}
