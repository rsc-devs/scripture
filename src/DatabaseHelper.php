<?php

namespace Drupal\scripture;

use Drupal\Core\Database\Database;
use PDO;

/**
 * Class DatabaseHelper.
 *
 * @package Drupal\scripture
 */
class DatabaseHelper {

  /**
   * Get a list of installed translations.
   *
   * FIXME: Since the scripture module is independent of living_word, we
   * should not use a "lw_" prefix for table names.
   *
   * @return array
   *   An array of Translation objects.
   */
  public static function getTranslations() {
    $select = Database::getConnection()
      ->select('lw_translations', 't')
      ->fields('t');
    return $select->execute()->fetchAll(
      PDO::FETCH_CLASS,
      'Drupal\scripture\Translation'
    );
  }

  /**
   * Get the info for a translation based on translation abbreviation.
   *
   * @param string $abbr
   *   The translation's abbreviation, which uniquely identifies it in the DB.
   *
   * @return Translation
   *   The translation object, loaded with the info from the DB.
   */
  public static function getTranslation($abbr) {
    $select = Database::getConnection()
      ->select('lw_translations', 't')
      ->fields('t')
      ->range(0, 1);
    $select->condition('abbr', $abbr);
    return $select->execute()->fetchAll(
      PDO::FETCH_CLASS,
      'Drupal\scripture\Translation'
    )[0];
  }

  /**
   * Retrieve verse from the DB based on verse ID.
   *
   * @param int $vid
   *   Verse ID.
   * @param string $translation_abbr
   *   The translation to load from. If not specified, the default will be used.
   *
   * @return Verse|false
   *   An array of verse info, or FALSE if an error occurs.
   */
  public static function getVerse($vid, $translation_abbr = NULL) {

    // Try to load the default translation if it is not specified.
    if (empty($translation_abbr)) {
      $translation_abbr = \Drupal::config('scripture.settings')
        ->get('default_translation');
    }

    // Check if a translation is specified.
    if (empty($translation_abbr)) {
      Common::showTranslationError();
      return FALSE;
    }
    else {
      $select = Database::getConnection()
        ->select('lw_verses', 'v')
        ->fields('v')
        ->range(0, 1);
      $select
        ->condition('vid', $vid)
        ->condition('translation', $translation_abbr);
      return $select->execute()->fetchAll(
        PDO::FETCH_CLASS,
        'Drupal\scripture\Verse'
      )[0];
    }

  }

  /**
   * Retrieve all verses in a range from the db based on vid.
   *
   * @param int $from_vid
   *   The id of the first verse.
   * @param int $to_vid
   *   The id of the last verse.
   * @param string $translation_abbr
   *   The translation. If omitted, the default translation will be used.
   *
   * @return array|false
   *   An array of Verse objects, or FALSE if an error occurs.
   */
  public static function getVersesFromRange(
    $from_vid,
    $to_vid,
    $translation_abbr = NULL
  ) {

    // Try to load the default translation if it is not specified.
    if (empty($translation_abbr)) {
      $translation_abbr = \Drupal::config('scripture.settings')->get('default_translation');
    }

    // Check if a translation is specified.
    if (empty($translation_abbr)) {
      Common::showTranslationError();
      return FALSE;
    }
    else {
      $select = Database::getConnection()
        ->select('lw_verses', 'v')
        ->fields('v')
        // Ordering by vid, since ordering versenum does not work, because
        // there are alphanumeric verses in some translations.
        ->orderBy('vid', 'ASC');
      $select
        ->condition('translation', $translation_abbr)
        ->condition('vid', [$from_vid, $to_vid], 'BETWEEN');
      return $select->execute()->fetchAll(
        PDO::FETCH_CLASS,
        'Drupal\scripture\Verse'
      );
    }

  }

  /**
   * Get all books for a specific translation.
   *
   * @param string $translation_abbr
   *   Will use a default translation if not given.
   *
   * @return array|false
   *   An array of books for this translation, or FALSE if an error occurs.
   */
  public static function getBooksInTranslation($translation_abbr = NULL) {
    if (empty($translation_abbr)) {
      $translation_abbr = \Drupal::config('scripture.settings')
        ->get('default_translation');
    }
    if (empty($translation_abbr)) {
      Common::showTranslationError();
      return FALSE;
    }

    $select = Database::getConnection()
      ->select('lw_books', 'b')
      ->fields('b')
      ->orderBy('booknum', 'ASC')
      ->condition('translation', $translation_abbr);
    return $select->execute()->fetchAll(
      PDO::FETCH_CLASS,
      'Drupal\scripture\Book'
    );
  }

  /**
   * Get a book based on the translation and book ordinal.
   *
   * @param int $booknum
   *   The ordinal of the required book in the specified translation.
   * @param string|null $translation_abbr
   *   The translation from which to retrieve the book. If not specified, the
   *   default translation will be used.
   *
   * @return Book|false
   *   A Book object.
   */
  public static function getBook($booknum, $translation_abbr = NULL) {
    if (empty($translation_abbr)) {
      $translation_abbr = \Drupal::config('scripture.settings')
        ->get('default_translation');
    }
    if (empty($translation_abbr)) {
      Common::showTranslationError();
      return FALSE;
    }

    $select = Database::getConnection()
      ->select('lw_books', 'b')
      ->fields('b')
      ->range(0, 1)
      ->condition('booknum', $booknum)
      ->condition('translation', $translation_abbr);

    return $select->execute()->fetchAll(
      PDO::FETCH_CLASS,
      'Drupal\scripture\Book'
    )[0];

  }

  /**
   * Retrieve a matching verse ID from the database.
   *
   * If translation is set, this function can be used to check if that verse
   * exists in a specific translation. Useful when validating user input.
   *
   * @param string $translation
   *   The bible translation to reference. The default will be
   *   loaded if this is NULL.
   * @param string $book_number
   *   The number of the book in that translation.
   * @param int $chapter_number
   *   The number of the chapter in that book.
   * @param string|int $verse_number
   *   The number of the verse in that chapter.
   *
   * @return int|null
   *   A verse id, or NULL on failure.
   */
  public static function getVerseId(
    $translation,
    $book_number,
    $chapter_number,
    $verse_number
  ) {
    if (empty($translation)) {
      $translation = \Drupal::config('scripture.settings')
        ->get('default_translation');
    }
    $select = Database::getConnection()->select('lw_verses', 'v')
      ->fields('v', array('vid'))
      ->range(0, 1);
    $select
      ->condition('translation', $translation)
      ->condition('booknum', $book_number)
      ->condition('chapternum', $chapter_number)
      ->condition('versenum', $verse_number);
    return $select->execute()->fetchField();
  }

}
