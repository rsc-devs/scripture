<?php

namespace Drupal\scripture\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\scripture\Common;
use Drupal\scripture\DatabaseHelper;

/**
 * Provides a form element to pick Bible verses.
 *
 * @FormElement("verse_picker")
 */
class VersePicker extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {

    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#theme' => 'verse_picker',
      '#process' => [
        [$class, 'processVersePicker'],
      ],
      '#pre_render' => [
        [$class, 'preRenderVersePicker'],
      ],
      '#element_validate' => [
        [$class, 'validate'],
      ],
    ];

  }

  /**
   * Prepare the render array for the template.
   *
   * @param array $element
   *   The element being processed.
   *
   * @return array
   *   The processed element.
   */
  public static function preRenderVersePicker(array $element) {
    return $element;
  }

  /**
   * Process the verse picker.
   *
   * Prepare default settings.
   * Prepare default values.
   * Prepare renderable elements.
   *
   * TODO: Making this element AJAX aware
   * See http://drupal.stackexchange.com/questions/162582 .
   * That will allow us to validate quickly, before the user submits the form.
   *
   * @param array $element
   *   The element being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param array $complete_form
   *   The current form.
   *
   * @return array
   *   The processed element.
   */
  public static function processVersePicker(
    array &$element,
    FormStateInterface $form_state,
    array &$complete_form
  ) {

    // Wrap the element in a div, to allow AJAX to update it.
    if (!$form_state->get('verse_picker_wrapper_id')) {
      // This runs with each ajax call, but the ajax id must be constant.
      // See http://drupal.stackexchange.com/questions/227137
      // TODO: If we will ever have more than one verse picker on the page, this
      // needs to get some unique value.
      $form_state->set('verse_picker_wrapper_id', 'verse-picker-ajax');
    }
    $element['#prefix'] = "<div id='{$form_state->get('verse_picker_wrapper_id')}'>";
    $element['#suffix'] = "</div>";

    $element['#tree'] = TRUE;

    // Get element settings and supply default settings.
    if (empty($element['#settings']) || !is_array($element['#settings'])) {
      $element['#settings'] = [];
    }
    $element['#settings'] += self::getDefaultSettings();

    // If the user is not allowed to change the translation or the book, then
    // those values are available in $element['#default_values'], but not in
    // $element['#values'] (not sure why). Copy the default values to
    // $element['#value'] if they are provided.
    if (isset($element['#default_value'])){
      $element['#value'] += $element['#default_value'];
    }

    $element['#suffix'] .= "<p>TODO: when translation changes, and chapters and verses are filled in, use verse IDs to update them intelligently.</p>";

    // If we don't have translations, we can't select verses.
    $translations = DatabaseHelper::getTranslations();
    if (empty($translations)) {
      Common::showTranslationError();
      return [
        '#type' => 'markup',
        '#markup' => t('No translations available. Please install a Bible module.'),
      ];
    }

    // Build a select list of translations.
    $translation_options = [];
    foreach ($translations as $t) {
      /* @var \Drupal\scripture\Translation $t */
      $translation_options[$t->abbr] = "{$t->abbr} / {$t->languageCode} / {$t->name}";
    }

    // Get the site-wide default translation.
    $global_default_translation = \Drupal::config('scripture.settings')->get('default_translation');
    if (empty($element['#value']['translation']) && !empty($global_default_translation)) {
      $element['#value']['translation'] = $global_default_translation;
    }

    $element['translation'] = [
      '#type' => 'select',
      '#title' => t('Translation:'),
      '#title_display' => 'before',
      '#ajax' => [
        'callback' => [self::class, 'ajaxTranslationChanged'],
        'wrapper' => $form_state->get('verse_picker_wrapper_id'),
        'effect' => 'fade',
      ],
      '#disabled' => !$element['#settings']['allow_translation'],
      // Translation is always required for the verse range to make sense.
      '#required' => TRUE,
      '#options' => $translation_options,
      '#default_value' => isset($element['#value']['translation']) ? $element['#value']['translation'] : NULL,
    ];

    if (!($element['#value']['translation'])) {
      // The translation is not selected yet, so we can't show the books.
      return $element;
    }

    // We have a pre-selected translation, so we can look for books.
    $books = DatabaseHelper::getBooksInTranslation($element['#value']['translation']);

    if (!is_array($books) || !count($books)) {

      // If there are no books, we can't select verses.
      $element['book'] = [
        '#type' => 'markup',
        '#markup' => t('No books were found.'),
      ];
      return $element;

    }

    // We now have a translation AND books. Build a list of options.
    $book_options = [];
    foreach ($books as $book) {
      /* @var \Drupal\scripture\Book $book */
      $book_options[] = "{$book->number}. {$book->name}";
    }

    $element['book'] = [
      '#type' => 'select',
      '#title' => t('Book:'),
      '#options' => $book_options,
      '#required' => TRUE,
      '#default_value' => isset($element['#value']['book']) ? $element['#value']['book'] : NULL,
      '#disabled' => !$element['#settings']['allow_book'],
    ];

    $element['from_chapter'] = [
      '#type' => 'textfield',
      '#size' => 3,
      '#maxlength' => 3,
      '#required' => FALSE,
      '#default_value' => isset($element['#value']['from_chapter']) ? $element['#value']['from_chapter'] : NULL,
      '#title_display' => 'invisible',
      '#theme_wrappers' => ['verse_picker_inline_wrapper'],
      '#placeholder' => t('ch.'),
    ];

    $element['from_verse'] = [
      '#type' => 'textfield',
      '#size' => 3,
      '#maxlength' => 3,
      '#required' => FALSE,
      '#default_value' => isset($element['#value']['from_verse']) ? $element['#value']['from_verse'] : NULL,
      '#title_display' => 'invisible',
      '#theme_wrappers' => ['verse_picker_inline_wrapper'],
      '#placeholder' => t('verse'),
    ];

    $element['to_chapter'] = [
      '#type' => 'textfield',
      '#title' => t('To chapter'),
      '#description' => t('Last chapter in the range. Leave blank to use the "from chapter" value.'),
      '#size' => 3,
      '#maxlength' => 3,
      '#required' => FALSE,
      '#default_value' => isset($element['#value']['to_chapter']) ? $element['#value']['to_chapter'] : NULL,
      '#access' => $element['#settings']['allow_range'],
      '#title_display' => 'invisible',
      '#theme_wrappers' => ['verse_picker_inline_wrapper'],
      '#placeholder' => t('ch.'),
    ];

    $element['to_verse'] = [
      '#type' => 'textfield',
      '#title' => t('To verse'),
      '#description' => t('Last verse in the range. Leave this and "To chapter" blank to use the "from verse" value.'),
      '#size' => 3,
      '#maxlength' => 3,
      '#required' => FALSE,
      '#default_value' => isset($element['#value']['to_verse']) ? $element['#value']['to_verse'] : NULL,
      '#access' => $element['#settings']['allow_range'],
      '#title_display' => 'invisible',
      '#theme_wrappers' => ['verse_picker_inline_wrapper'],
      '#placeholder' => t('verse'),
    ];

    if ($element['#settings']['allow_range']) {
      $element['from_chapter']['#title'] = t('From chapter');
      $element['from_chapter']['#description'] = t('First chapter in the range.');
      $element['from_verse']['#title'] = t('From verse');
      $element['from_verse']['#description'] = t('First verse in the range.');
    }
    else {
      $element['from_chapter']['#title'] = t('Chapter');
      $element['from_verse']['#title'] = t('Verse');
    }

    return $element;
  }

  /**
   * AJAX event handler for the translation select element.
   *
   * The translation changed, so we need to get a new list of books.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form element to replace.
   */
  public static function ajaxTranslationChanged(array &$form, FormStateInterface $form_state) {
    $array_parents = $form_state->getTriggeringElement()['#array_parents'];
    // Go one level up from the triggering element.
    array_pop($array_parents);
    $scripture_verse_picker_element = NestedArray::getValue(
      $form,
      $array_parents
    );
    return $scripture_verse_picker_element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {

    if ($input === FALSE) {

      $input = isset($element['#default_value']) ? $element['#default_value'] : array();

    }
    else {

      if (empty($input['to_chapter'])) {
        $input['to_chapter'] = $input['from_chapter'];
        if (empty($input['to_verse'])) {
          $input['to_verse'] = $input['from_verse'];
        }
      }

    }

    return $input;

  }

  /**
   * Validate the input.
   *
   * Also:
   * - Check the verse ranges logically.
   * - Check if verses exist in database.
   * - Load verse ids from database.
   *
   * @param array $element
   *   The element being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param array $complete_form
   *   The current form.
   */
  public static function validate(
    array &$element,
    FormStateInterface $form_state,
    array &$complete_form
  ) {

    // If the element is empty and not required, then all is well.
    if (self::isEmpty($element) && !$element['#required']) {
      return;
    } // else further inspection is required.

    $v = &$element['#value'];
    $errors = FALSE;

    // Check that a starting verse and chapter is selected.
    if (empty($v['from_chapter'])) {
      $form_state->setError($element, t('Specify a starting chapter.'));
      $errors = TRUE;
    }
    if (empty($v['from_verse'])) {
      $form_state->setError($element, t('Specify a starting verse.'));
      $errors = TRUE;
    }

    // Check that verse accompanies chapter.
    if (!empty($v['to_chapter']) && empty($v['to_verse'])) {
      $form_state->setError(
        $element,
        t('You filled in a chapter, but not a verse.')
      );
      $errors = TRUE;
    }

    if (empty($v['to_chapter'])) {

      // Use the from_chapter value if to_chapter was empty.
      $v['to_chapter'] = $v['from_chapter'];

      if (empty($v['to_verse'])) {
        // Use the from_verse value if both to_chapter and to_verse were empty.
        $v['to_verse'] = $v['from_verse'];
      }

    }

    // Check that the verse range is positive.
    if (
      $v['from_chapter'] > $v['to_chapter'] ||
      (
        $v['from_chapter'] == $v['to_chapter'] &&
        $v['from_verse'] > $v['to_verse']
      )
    ) {
      $form_state->setError(
        $element,
        t('First verse and chapter must be before the last verse and chapter.')
      );
      $errors = TRUE;
    }

    /*
     * If there are no other errors, check if the verses exist by requesting
     * their verse ids. We need to check for non-empty parameters, because
     * the "required" validation criteria is not checked on individual
     * elements when this is called with AJAX.
     */
    if (!$errors && self::isPopulated($element)) {

      // Get the verse ids using the above information.
      $v['from_vid'] = DatabaseHelper::getVerseId(
        $v['translation'],
        $v['book'],
        $v['from_chapter'],
        $v['from_verse']
      );
      $v['to_vid'] = DatabaseHelper::getVerseId(
        $v['translation'],
        $v['book'],
        $v['to_chapter'],
        $v['to_verse']
      );

      // Check if the verses exist, i.e. if they have verse ids.
      if (!($v['from_vid'] && $v['to_vid'])) {
        // At least one verse in the range does not exist.
        $form_state->setError(
          $element,
          t('Requested verse does not exist.')
        );
      }

    }

    // Update the form state with the new values.
    $form_state->setValueForElement($element, $v);

  }

  /**
   * Check if element is empty.
   *
   * NOTE: This is NOT the same as !isPopulated($element).
   *
   * @param array $element
   *   The element to check.
   *
   * @return bool
   *   TRUE if all chapter numbers and verse numbers are empty.
   */
  private static function isEmpty(array $element) {
    return
      empty($element['#value']['from_chapter']) &&
      empty($element['#value']['from_verse']) &&
      empty($element['#value']['to_chapter']) &&
      empty($element['#value']['to_verse']);
  }

  /**
   * Check if element is fully populated.
   *
   * NOTE: This is NOT the same as !isEmpty($element).
   *
   * @param array $element
   *   The element to check.
   *
   * @return bool
   *   TRUE if all chapter numbers and verse numbers are filled in.
   */
  private static function isPopulated(array $element) {
    return
      !empty($element['#value']['from_chapter']) &&
      !empty($element['#value']['from_verse']) &&
      !empty($element['#value']['to_chapter']) &&
      !empty($element['#value']['to_verse']);
  }

  /**
   * Get the default settings for the verse picker.
   *
   * This is also used in the ScriptureReferenceWidget.
   *
   * @return array
   *   An array of settings, keyed by name.
   */
  public static function getDefaultSettings() {
    return [
      // Translation cannot be changed if allow_translation is FALSE.
      'allow_translation' => TRUE,
      // Book cannot be changed if allow_book is FALSE.
      'allow_book' => TRUE,
      // Only a single verse may be selected if allow_range FALSE.
      'allow_range' => TRUE,
    ];
  }

}
