<?php

namespace Drupal\scripture\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Class ScriptureCallbackQueries.
 *
 * @package Drupal\scripture\Form
 */
class QueryConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'scripture_callback_queries';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('scripture.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['scripture.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('scripture.settings');

    /* TODO: insert a picture to explain these two options. */

    $form = [];

    $form['match_quality_weight_query'] = [
      '#type' => 'textfield',
      '#size' => 3,
      '#title' => t('Query match quality weight'),
      '#description' => t('The relative importance of search results covering the entire query.'),
      '#required' => TRUE,
      '#default_value' => $config->get('match_quality_weight_query'),
    ];

    $form['match_quality_weight_entity'] = [
      '#type' => 'textfield',
      '#size' => 3,
      '#title' => t('Entity match quality weight'),
      '#description' => t('The relative importance of the query covering the entire range of verses in the search results.'),
      '#required' => TRUE,
      '#default_value' => $config->get('match_quality_weight_entity'),
    ];

    return parent::buildForm($form, $form_state);
  }

}
