<?php

namespace Drupal\scripture\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\scripture\DatabaseHelper;

/**
 * Class TranslationConfigForm.
 *
 * @package Drupal\scripture\Form
 */
class TranslationConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'scripture_callback_translations';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('scripture.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['scripture.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('scripture.settings');

    $form = [];

    $translations = DatabaseHelper::getTranslations();

    if (empty($translations)) {
      // TODO: prevent "no_translations: null" from being saved in
      // "scripture.config". Not a problem, but not very elegant.
      $form['no_translations'] = [
        '#type' => 'markup',
        '#markup' => $this->t('There are no installed translations.'),
      ];
      $form['default_translation'] = [
        '#type' => 'value',
        '#value' => NULL,
      ];
    }
    else {

      // Build a select list of translations.
      $translation_options = [];
      foreach ($translations as $t) {
        /* @var \Drupal\scripture\Translation $t */
        $translation_options[$t->abbr] = "{$t->abbr} / {$t->languageCode} / {$t->name}";
      }

      $form['default_translation'] = [
        '#type' => 'select',
        '#title' => t('Default translation'),
        '#options' => $translation_options,
        '#description' => t('Choose the default translation to use when none is specified.'),
        '#required' => TRUE,
        '#default_value' => [
          $config->get('default_translation'),
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

}
