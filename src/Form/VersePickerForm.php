<?php

namespace Drupal\scripture\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VersePickerForm.
 *
 * @package Drupal\scripture\Form
 */
class VersePickerForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'verse_picker_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['pick_a_verse'] = [
      '#type' => 'verse_picker',
      '#title' => $this->t('Pick a verse'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
