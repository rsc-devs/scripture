<?php

namespace Drupal\scripture\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\scripture\Common;
use Drupal\scripture\Plugin\Field\FieldType\ScriptureReferenceItem;

/**
 * Plugin implementation of the 'scripture_reference' formatter.
 *
 * @FieldFormatter(
 *   id = "scripture_reference",
 *   label = @Translation("Scripture reference"),
 *   field_types = {
 *     "scripture_reference"
 *   }
 * )
 */
class ScriptureReferenceFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'show_text' => Common::SHOW_TEXT_NO,
      'show_ref' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'show_ref' => [
        '#type' => 'checkbox',
        '#title' => t('Show verse reference'),
        '#required' => FALSE,
        '#default_value' => $this->getSetting('show_ref'),
      ],
      'show_text' => [
        '#type' => 'select',
        '#title' => t('Show verse text?'),
        '#options' => [
          Common::SHOW_TEXT_NO => "No",
          Common::SHOW_TEXT_TRUNC => "First and last few words",
          Common::SHOW_TEXT_ALL => "Entire text",
        ],
        '#required' => TRUE,
        '#default_value' => $this->getSetting('show_text'),
      ],
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();

    if ($settings['show_ref']) {
      $show_ref = $this->t("Show verse reference.");
    }
    else {
      $show_ref = $this->t("Do not show verse reference.");
    }
    switch ($settings['show_text']) {
      case Common::SHOW_TEXT_NO:
        $show_text = $this->t("Do not show verse text.");
        break;

      case Common::SHOW_TEXT_TRUNC:
        $show_text = $this->t("Show first and last few words of text range.");
        break;

      case Common::SHOW_TEXT_ALL:
        $show_text = $this->t("Show the entire text.");
        break;

      default:
        $show_text = $this->t("Invalid setting specified.");
    }

    return [
      $show_ref,
      $show_text,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue($item);
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field value.
   *
   * @param \Drupal\scripture\Plugin\Field\FieldType\ScriptureReferenceItem $item
   *   One field item.
   *
   * @return array
   *   A render array which shows the formatted field value.
   */
  protected function viewValue(ScriptureReferenceItem $item) {
    $settings = $this->getSettings();
    $output = [];

    if ($settings['show_ref']) {
      // Build the human-readable scripture reference.
      $output['ref'] = [
        '#prefix' => '<cite>',
        '#markup' => (string) $item,
        '#suffix' => '</cite>',
      ];
    }

    switch ($settings['show_text']) {
      case Common::SHOW_TEXT_TRUNC:
        // Show a shortened version of the text, using bits of the first and
        // last verses.
        $markup = scripture_shorten(
          $item->getFirstVerse()->text,
          $item->getLastVerse()->text
        );
        $output['text'] = [
          '#prefix' => '<q>',
          '#markup' => $markup,
          '#suffix' => '</q>',
        ];
        break;

      case Common::SHOW_TEXT_ALL:

        // Pull all of the verses into one long string.
        $all_text = "";
        foreach ($item->getAllVerses() as $verse) {
          $all_text .= $verse->text . " ";
        }

        $output['text'] = [
          '#prefix' => '<blockquote>',
          '#markup' => substr($all_text, 0, -1),
          '#suffix' => '</blockquote>',
        ];

        break;
    }

    return $output;
  }

}
