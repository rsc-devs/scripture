<?php

namespace Drupal\scripture\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\scripture\DatabaseHelper;

/**
 * Plugin implementation of the 'scripture_reference' field type.
 *
 * @FieldType(
 *   id = "scripture_reference",
 *   label = @Translation("Scripture reference"),
 *   description = @Translation("A reference to a range of verses from a Bible translation."),
 *   default_widget = "scripture_reference",
 *   default_formatter = "scripture_reference"
 * )
 */
class ScriptureReferenceItem extends FieldItemBase {

  /**
   * The first verse in the range.
   *
   * @var \Drupal\scripture\Verse
   */
  private $fromVerse;

  /**
   * The last verse in the range.
   *
   * @var \Drupal\scripture\Verse
   */
  private $toVerse;

  /**
   * The translation used in the verse range.
   *
   * @var \Drupal\scripture\Translation
   */
  private $translation;

  /**
   * The book used in the verse range.
   *
   * @var \Drupal\scripture\Book
   */
  private $book;

  /**
   * An array of all verses in the range.
   *
   * @var array
   */
  private $allVerses;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      // We have no settings.
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['from_vid'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Verse ID of the first verse in the range'))
      ->setRequired(TRUE);

    $properties['to_vid'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Verse ID of the last verse in the range'))
      ->setRequired(TRUE);

    $properties['translation'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('The abbreviation of the specific bible translation referenced by this field'))
      ->addConstraint('Length', [
        'max' => 8,
      ])
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'from_vid' => [
          'description' => 'Verse ID of the first verse in the range',
          'type' => 'int',
          'not-null' => TRUE,
          'unsigned' => TRUE,
        ],
        'to_vid' => [
          'description' => 'Verse ID of the last verse in the range',
          'type' => 'int',
          'not-null' => TRUE,
          'unsigned' => TRUE,
        ],
        'translation' => [
          'description' => 'The abbreviation of the specific bible translation referenced by this field',
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => 8,
          // TODO: foreign key with lw_translations table?
        ],
      ],
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();
    // No constraints.
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    // $random = new Random();
    // TODO: modify stub.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];
    // We have no settings.
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // Even for a single verse, both from_vid and to_vid are stored,
    // so use || instead of && to check for an empty field.
    try {
      return empty($this->get('from_vid')->getValue())
        || empty($this->get('to_vid')->getValue());
    }
    catch (MissingDataException $e) {
      return TRUE;
    }
  }

  /**
   * Get the first verse in the range.
   *
   * This is lazy-loaded from the DB, and cached.
   *
   * @return \Drupal\scripture\Verse|false
   *   The first verse in the range, or FALSE if we can't load it.
   */
  public function getFirstVerse() {
    try {
      $from_vid = $this->get('from_vid')->getValue();
    }
    catch (MissingDataException $e) {
      $from_vid = NULL;
    }
    // If there is no from_vid, we can't load a verse.
    if (empty($from_vid)) {
      return FALSE;
    }

    try {
      $translation_abbr = $this->get('translation')->getValue();
    }
    catch (MissingDataException $e) {
      $translation_abbr = NULL;
    }

    // If we have not loaded the full verse from the DB, try to do so now.
    if (empty($this->fromVerse)) {
      $this->fromVerse = DatabaseHelper::getVerse($from_vid, $translation_abbr);
    }

    return $this->fromVerse;
  }

  /**
   * Get the last verse in the range.
   *
   * This is lazy-loaded from the DB, and cached.
   *
   * @return \Drupal\scripture\Verse|false
   *   The last verse in the range, or FALSE if we can't load it.
   */
  public function getLastVerse() {
    try {
      $to_vid = $this->get('to_vid')->getValue();
    }
    catch (MissingDataException $e) {
      $to_vid = NULL;
    }
    // If there is no to_vid, we can't load a verse.
    if (empty($to_vid)) {
      return FALSE;
    }

    try {
      $from_vid = $this->get('from_vid')->getValue();
    }
    catch (MissingDataException $e) {
      $from_vid = NULL;
    }
    // If from_vid and to_vid are the same, just return the first verse.
    if ($from_vid == $to_vid) {
      return $this->getFirstVerse();
    }

    try {
      $translation_abbr = $this->get('translation')->getValue();
    }
    catch (MissingDataException $e) {
      $translation_abbr = NULL;
    }

    // If we have not loaded the full verse from the DB, try to do so now.
    if (empty($this->toVerse)) {
      $this->toVerse = DatabaseHelper::getVerse($to_vid, $translation_abbr);
    }

    return $this->toVerse;
  }

  /**
   * Get all verses in this range.
   *
   * This is lazy-loaded from the DB, and cached.
   *
   * @return array|false
   *   An array of Verse objects, or false if we can't load them.
   */
  public function getAllVerses() {
    if ($this->isEmpty()) {
      return FALSE;
    }

    // If we have not loaded this from the DB, do that now.
    if (empty($this->allVerses)) {

      // Try to get the first vid.
      try {
        $from_vid = $this->get('from_vid')->getValue();
      }
      catch (MissingDataException $e) {
        return FALSE;
      }

      // Try to get the last vid.
      try {
        $to_vid = $this->get('to_vid')->getValue();
      }
      catch (MissingDataException $e) {
        return FALSE;
      }

      // If from_vid and to_vid are the same there is just one verse.
      if ($from_vid == $to_vid) {
        $this->allVerses = [
          $this->getFirstVerse(),
        ];
      }
      else {

        // Get the translation, if any.
        try {
          $translation_abbr = $this->get('translation')->getValue();
        }
        catch (MissingDataException $e) {
          $translation_abbr = NULL;
        }

        // Load a range of verses from the DB.
        $this->allVerses = DatabaseHelper::getVersesFromRange(
          $from_vid,
          $to_vid,
          $translation_abbr
        );

        // Optimisation: since we have the first and last verses now, we might
        // as well cache them now. We assume the array is numerically keyed.
        if (empty($this->fromVerse)) {
          $this->fromVerse = $this->allVerses[0];
        }
        if (empty($this->toVerse)) {
          $this->toVerse = $this->allVerses[count($this->allVerses) - 1];
        }

      }
    }

    return $this->allVerses;
  }

  /**
   * Get the referenced translation, if any.
   *
   * This is lazy-loaded from the DB, and cached.
   *
   * @return null|\Drupal\scripture\Translation
   *   The referenced translation, or NULL if there is none.
   */
  public function getTranslation() {
    try {
      $translation_abbr = $this->get('translation')->getValue();
    }
    catch (MissingDataException $e) {
      return NULL;
    }

    // If we have not loaded the translation from the DB, try to do so now.
    if (empty($this->translation)) {
      $this->translation = DatabaseHelper::getTranslation($translation_abbr);
    }

    return $this->translation;
  }

  /**
   * Get the book in which the current verse range resides.
   *
   * We don't support ranges across books, so the first and last verses should
   * be in the same book.
   *
   * This is lazy-loaded from the DB, and cached.
   *
   * @return \Drupal\scripture\Book
   *   The book containing the verse range.
   */
  public function getBook() {

    if (empty($this->book)) {
      $this->book = $this->getFirstVerse()->getBook();
    }

    return $this->book;
  }

  /**
   * Generate a human-readable verse range.
   *
   * @return string
   *   Human-readable verse range, like "Genesis 1:1".
   */
  public function __toString() {

    $first_verse = $this->getFirstVerse();
    $last_verse = $this->getLastVerse();
    $book_name = $this->getBook()->name;

    if ($first_verse->vid == $last_verse->vid) {
      // E.g. "Genesis 1:1".
      $str = "{$book_name} {$first_verse->chapterNumber}:{$first_verse->number}";
    }
    else {
      // NB: "En dashes" are used to indicate verse ranges, not hyphens.
      if ($first_verse->chapterNumber == $last_verse->chapterNumber) {
        // NB: No spaces around the dash when range is within a chapter.
        // E.g. "Genesis 1:1–2".
        $str = "{$book_name} {$first_verse->chapterNumber}:{$first_verse->number}–{$last_verse->number}";
      }
      else {
        // NB: spaces around the dash when going across chapters.
        // E.g. "Genesis 1:1 – 2:1".
        $str = "{$book_name} {$first_verse->chapterNumber}:{$first_verse->number} – {$last_verse->chapterNumber}:{$last_verse->number}";
      }
    }

    if (!empty($translation_abbr)) {
      $translation = DatabaseHelper::getTranslation($translation_abbr);
      $str .= " (<abbr title='{$translation->name}'>" . strtoupper($translation->abbr) . "</abbr>)";
    }

    return $str;

  }

}
