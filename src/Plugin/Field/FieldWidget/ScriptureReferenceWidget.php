<?php

namespace Drupal\scripture\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\scripture\Common;
use Drupal\scripture\Element\VersePicker;

/**
 * Plugin implementation of the 'scripture_reference' widget.
 *
 * @FieldWidget(
 *   id = "scripture_reference",
 *   label = @Translation("Scripture reference picker"),
 *   description = @Translation("A compound form element for editing scripture references"),
 *   field_types = {
 *     "scripture_reference"
 *   },
 *   multiple_values = FALSE,
 * )
 */
class ScriptureReferenceWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [

      // If this is false, only verse IDs will be saved.
      // The VersePicker has no such option, since you need a translation to
      // determine the verse IDs.
      'save_translation' => FALSE,

    ] + VersePicker::getDefaultSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   *
   * TODO: Add option to specify the default book. The field only saves the
   * verse IDs, and optionally the translation, so the default book can't be
   * specified at the field level.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [
      'save_translation' => [
        '#type' => 'radios',
        '#title' => $this->t('Save translation'),
        '#default_value' => $this->getSetting('save_translation'),
        '#options' => array(
          FALSE => $this->t('Save only the verse IDs.'),
          TRUE => $this->t('Save the selected translation with the verse IDs.'),
        ),
        '#description' => $this->t('This setting is useful when you want the field to refer to a specific Bible verse, but no a specific translation. In both cases, the widget will still show a translation picker, since the chapter and verse numbers only make sense in the context of a specific translation.'),
      ],
      'allow_translation' => [
        '#type' => 'radios',
        '#title' => $this->t('Allow translation'),
        '#default_value' => $this->getSetting('allow_translation'),
        '#options' => array(
          FALSE => $this->t('The user may not change the translation. The default translation will be used (configured site-wide.'),
          TRUE => $this->t('The user may select a translation.'),
        ),
      ],
      'allow_book' => [
        '#type' => 'radios',
        '#title' => $this->t('Allow book'),
        '#default_value' => $this->getSetting('allow_book'),
        '#options' => array(
          FALSE => $this->t('The user may not change the selected book.'),
          TRUE => $this->t('The user may select a book.'),
        ),
      ],
      'allow_range' => [
        '#type' => 'radios',
        '#title' => $this->t('Allow range'),
        '#default_value' => $this->getSetting('allow_range'),
        '#options' => array(
          FALSE => $this->t('The user may select one verse.'),
          TRUE => $this->t('The user may select a range of verses.'),
        ),
      ],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [
      $this->getSetting('save_translation')
        ? $this->t('The selected translation will be saved along with the verse IDs.')
        : $this->t('Only the verse IDs will be saved.'),
      $this->getSetting('allow_translation')
        ? $this->t('The user may select a translation.')
        : $this->t('The user may not change the selected translation.'),
      $this->getSetting('allow_book')
        ? $this->t('The user may select a book.')
        : $this->t('The user may not change the selected book.'),
      $this->getSetting('allow_range')
        ? $this->t('The user may select a range of verses.')
        : $this->t('The user may select one verse.'),
    ];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    // Use fieldset to show the title and put a nice frame around it.
    $element['#type'] = 'fieldset';

    $element['verse'] = array(
      '#type' => 'verse_picker',

      // Three of the settings defined in the widget are actually settings for
      // the verse picker. For some reason, $this->getSettings() returns "0" and
      // "1" instead of FALSE and TRUE. This causes problems in the verse
      // picker, since '#access' => "0" is considered TRUE. We fix that here.
      '#settings' => [
        'allow_translation' => $this->getSetting('allow_translation') ? TRUE : FALSE,
        'allow_book' => $this->getSetting('allow_book') ? TRUE : FALSE,
        'allow_range' => $this->getSetting('allow_range') ? TRUE : FALSE,
      ],

      // TODO: how should we handle #required?
      // '#required' => $instance['required'] && $delta == 0,

    );

    /* @var \Drupal\scripture\Plugin\Field\FieldType\ScriptureReferenceItem $item */
    $item = &$items[$delta];

    // If the item is not empty, populate the form element.
    if (!$item->isEmpty()) {

      // First try to get the translation from the field.
      try {
        $translation_abbr = $item->get('translation')->getValue();
      }
      catch (MissingDataException $e) {
        $translation_abbr = NULL;
      }

      // If the field does not specify a translation, use the default.
      if (empty($translation_abbr)) {
        $translation_abbr = \Drupal::config('scripture.settings')
          ->get('default_translation');
      }

      // If there is still no translation specified, we can't load the verses.
      if (empty($translation_abbr)) {
        Common::showTranslationError();
      }
      else {

        $from_verse = $item->getFirstVerse();
        $to_verse = $item->getLastVerse();

        $element['verse']['#default_value'] = array(
          'translation' => $translation_abbr,
          'book' => $from_verse->bookNumber,
          'from_chapter' => $from_verse->chapterNumber,
          'from_verse' => $from_verse->number,
          'to_chapter' => $to_verse->chapterNumber,
          'to_verse' => $to_verse->number,
        );

      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    // Check whether we should save the translation.
    $save_translation = $this->getSetting('save_translation');

    foreach ($values as $delta => &$value) {
      // Place the values that should end up in the FieldItem's properties
      // directly under the delta. See Map:setValue() and this question:
      // https://drupal.stackexchange.com/questions/268803 .
      $value['from_vid'] = isset($value['verse']['from_vid']) ? $value['verse']['from_vid'] : NULL;
      $value['to_vid'] = isset($value['verse']['to_vid']) ? $value['verse']['to_vid'] : NULL;
      $value['translation'] = ($save_translation && isset($value['verse']['translation'])) ? $value['verse']['translation'] : NULL;
    }

    // No call to parent needed.
    return $values;
  }

}
