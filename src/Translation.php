<?php

namespace Drupal\scripture;

/**
 * Translation class.
 *
 * Represents a translation loaded from the DB.
 *
 * @package Drupal\scripture
 */
class Translation {

  /**
   * The abbreviation of this translation.
   *
   * This is a unique key, identifying the translation in the DB.
   *
   * @var string
   */
  public $abbr;

  /**
   * The language code of this translation.
   *
   * This corresponds to language codes used elsewhere in Drupal.
   *
   * @var string
   */
  public $languageCode;

  /**
   * The 'friendly' name of this translation, used in the GUI.
   *
   * @var string
   */
  public $name;

  /**
   * Map DB column names to object properties.
   *
   * This magic function is called when the DatabaseHelper tries to set
   * inaccessible properties on a Translation object. That happens when the name
   * of the property in the DB (which generally follows snake_case_naming) is
   * not the same as the name of the property in this class (which should follow
   * camelCaseNaming).
   *
   * @param string $name
   *   The name of the property (column name) being set.
   * @param mixed $value
   *   The value of the property being set.
   */
  public function __set($name, $value) {
    switch ($name) {
      case 'lang':
        $this->languageCode = $value;
        break;
    }
  }

  /**
   * Get the names and order of books in this translation.
   *
   * @return array
   *   An array of Book objects, keyed by booknumber, in the order they appear
   *   in the current translation.
   */
  public function getBooks() {
    return DatabaseHelper::getBooksInTranslation($this->abbr);
  }

  public function getVerse() {
    // todo
  }

  public function getVerses() {
    // todo
  }

}
