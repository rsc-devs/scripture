<?php

namespace Drupal\scripture;

/**
 * Verse class.
 *
 * Represents a Bible verse loaded from the DB.
 *
 * @package Drupal\scripture
 */
class Verse {

  /**
   * Verse ID.
   *
   * This uniquely identifies the verse across translations. All translations of
   * a verse will have the same vid, although the bookNumber, chapterNumber,
   * verseNumber and verseText may differ.
   *
   * Verse IDs will always be in order. Sorting by Verse ID should always result
   * in a canonical ordering of verses within a book. However, since the order
   * of books may vary from one translation to the next, Verse IDs should not be
   * used to sort verses from disparate books.
   *
   * TODO: say something about the sparsity and somewhat-regular spacing of
   * Verse IDs
   *
   * TODO: what about compound verses? Some translations combine verses.
   *
   * @var int
   */
  public $vid;

  /**
   * The abbreviation of the translation of this verse.
   *
   * This is a unique key, identifying the translation in the DB.
   *
   * @var string
   */
  public $translationAbbr;

  /**
   * The ordinal of the book in which this verse resides.
   *
   * This number, together with the translation abbreviation, uniquely
   * identifies the book in the DB. Books are ordered differently in different
   * translations.
   *
   * @var int
   */
  public $bookNumber;

  /**
   * The ordinal of the chapter of the book in which this verse resides.
   *
   * @var int
   */
  public $chapterNumber;

  /**
   * The ordinal of this verse in the chapter.
   *
   * This is a string instead of an int, because some translations have verses
   * called "1–2" or "1a".
   *
   * @var string
   */
  public $number;

  /**
   * The translated text contained in this verse.
   *
   * @var string
   */
  public $text;

  /**
   * Map DB column names to object properties.
   *
   * This magic function is called when the DatabaseHelper tries to set
   * inaccessible properties on a Translation object. That happens when the name
   * of the property in the DB (which generally follows snake_case_naming) is
   * not the same as the name of the property in this class (which should follow
   * camelCaseNaming).
   *
   * @param string $name
   *   The name of the property (column name) being set.
   * @param mixed $value
   *   The value of the property being set.
   */
  public function __set($name, $value) {
    switch ($name) {
      case 'translation':
        $this->translationAbbr = $value;
        break;

      case 'booknum':
        $this->bookNumber = $value;
        break;

      case 'chapternum':
        $this->chapterNumber = $value;
        break;

      case 'versenum':
        $this->number = $value;
        break;

      case 'versetext':
        $this->text = $value;
        break;
    }
  }

  /**
   * Load the translation for this verse from the DB.
   *
   * @return Translation
   *   The Translation object corresponding to $this->translationAbbr.
   */
  public function getTranslation() {
    return DatabaseHelper::getTranslation($this->translationAbbr);
  }

  /**
   * Load the book in which this verse resides.
   *
   * @return Book
   *   The Book object representing the book containing this verse.
   */
  public function getBook() {
    return DatabaseHelper::getBook($this->bookNumber, $this->translationAbbr);
  }

}
